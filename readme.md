**About**

This a telegram bot `@musstreamingbot`

It converts links from one music streaming service to others.
Sometimes it will generate direct urls to same item, but in most cases _for now_ just links to search page. Refer to supported services section.

**Usage**

Inline mode:
+ Enter one of the supported urls to a song, artist or an album: `@musstreamingbot https://open.spotify.com/track/6l2YlgrnMHCHUqPGs8kvwk`
+ Enter some keywords to search music by: `@musstreamingbot Solstafir Silfur-Refur`

![inline mode example](readme/inline1.png "Usage example")

Group chats:

Add a bot and it will convert urls for some services on the fly.

**Supported services**

|  Service   | Search page url | Direct item link | Extract metdata from url |  Search by keywords | Connected to service API |
|:-----------|:---------------:|:----------------:|:------------------------:|:-------------------:|:------------------------:|
| Spotify    |      +          |        +         |              +           |            +        |             +            |
| Deezer     |      +          |        -         |              +           |            -        |             -            |
| Itunes     |      -          |        -         |              +           |            -        |             -            |
| Youtube    |      +          |        -         |              -           |            -        |             -            |
| Youtube Music |      +          |        -         |              -           |            -        |             -            |
| Apple Music |      +          |        -         |              +           |            -        |             -            |

Atleast for all services listed above basic url parsing is working.
