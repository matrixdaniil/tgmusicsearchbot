package tgmusicbot

import org.junit.Test
import kotlin.test.assertEquals

class FallbackPageParserTest {

    val fallbackParser = FallbackPageParser()

    @Test
    fun testDeezerAlbumPage() {
        val etalonInfo = MusicMeta.Album(artist = "Sólstafir",
                album = "Berdreyminn",
                cover = "https://cdns-images.dzcdn.net/images/cover/b0bedd06312c699076bfabf1f94ea55a/500x500.jpg")

        val info = fallbackParser
                .parse(HtmlDocumentLoader().load("deezer-album"))
        assertEquals(actual = info, expected = FallbackPageParser.ParseResult.Supported(etalonInfo))
    }

    @Test
    fun testSpotifyAlbumPage() {
        val etalonInfo = MusicMeta.Album(artist = "Sólstafir",
                album = "Berdreyminn",
                cover = "https://i.scdn.co/image/ab67616d0000b273ed6d0cb3f3bea5ba32c8f49a")

        val info = fallbackParser
                .parse(HtmlDocumentLoader().load("spotify-album"))
        assertEquals(actual = info, expected = FallbackPageParser.ParseResult.Supported(etalonInfo))
    }

    @Test
    fun testDeezerTrackPage() {
        val etalonInfo = MusicMeta.Track(artist = "Sólstafir",
                track = "Ísafold",
                duration = 298,
                cover = "https://e-cdns-images.dzcdn.net/images/cover/b0bedd06312c699076bfabf1f94ea55a/500x500.jpg")

        val info = fallbackParser
                .parse(HtmlDocumentLoader().load("deezer-track"))
        assertEquals(actual = info, expected = FallbackPageParser.ParseResult.Supported(etalonInfo))
    }

    @Test
    fun testSpotifyTrackPage() {
        val etalonInfo = MusicMeta.Track(artist = "Sólstafir",
                track = "Ísafold",
                duration = 299,
                cover = "https://i.scdn.co/image/ab67616d0000b273ed6d0cb3f3bea5ba32c8f49a")

        val info = fallbackParser
                .parse(HtmlDocumentLoader().load("spotify-track"))
        assertEquals(actual = info, expected = FallbackPageParser.ParseResult.Supported(etalonInfo))
    }

    @Test
    fun testDeezerArtistPage() {
        val etalonInfo = MusicMeta.Artist(artist = "Sólstafir",
                cover = "https://e-cdns-images.dzcdn.net/images/artist/3d26e28061e13e31392eec189bfab183/500x500.jpg")

        val info = fallbackParser
                .parse(HtmlDocumentLoader().load("deezer-artist"))
        assertEquals(actual = info, expected = FallbackPageParser.ParseResult.Supported(etalonInfo))
    }

    @Test
    fun testSpotifyArtistPage() {
        val etalonInfo = MusicMeta.Artist(artist = "Sólstafir",
                cover = "https://i.scdn.co/image/f0bb78e9c09e269341df4f0019571a15565a958f")

        val info = fallbackParser
                .parse(HtmlDocumentLoader().load("spotify-artist"))
        assertEquals(actual = info, expected = FallbackPageParser.ParseResult.Supported(etalonInfo))
    }
}