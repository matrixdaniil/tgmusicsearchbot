package tgmusicbot


import kotlin.test.*
import tgmusicbot.itunes.ItunesArtistPageParser
import tgmusicbot.itunes.ItunesAlbumTrackPageParser

class ItunesPageParserTest {

    @Test
    fun parseArtistPage() {
        val etalonInfo = MusicMeta.Artist(artist = "‎Solstafir",
                cover = "https://is5-ssl.mzstatic.com/image/thumb/Music113/v4/56/9b/51/569b5160-b3d9-e037-cb1a-3ef78951c276/886443775442.jpg/1200x630cw.png")

        val info = ItunesArtistPageParser()
                .parse(HtmlDocumentLoader().load("itunes-artist"))
        assertEquals(actual = info, expected = etalonInfo)
    }

    @Test
    fun parseAlbumnPage() {
        val etalonInfo = MusicMeta.Album(artist = "Solstafir",
                album = "‎Berdreyminn",
                cover = "https://is3-ssl.mzstatic.com/image/thumb/Music124/v4/03/b7/59/03b759f5-dc2b-e767-efce-8224e63dfff4/886446397894.jpg/600x600bf.png")

        val info = ItunesAlbumTrackPageParser()
                .parse(HtmlDocumentLoader().load("itunes-album"))
        assertEquals(actual = info, expected = etalonInfo)
    }

    @Test
    fun parseTrackPage() {
        val etalonInfo = MusicMeta.Track(artist = "Solstafir",
                track = "‎Ísafold",
                duration = 298,
                cover = "https://is3-ssl.mzstatic.com/image/thumb/Music124/v4/03/b7/59/03b759f5-dc2b-e767-efce-8224e63dfff4/886446397894.jpg/600x600bf.png")

        val info = ItunesAlbumTrackPageParser()
                .parse(HtmlDocumentLoader().load("itunes-track"))
        assertEquals(actual = info, expected = etalonInfo)
    }

    @Test
    fun parseAppleMusicArtistPage() {
        val etalonInfo = MusicMeta.Artist(artist = "In Flames",
                cover = "https://is5-ssl.mzstatic" +
                        ".com/image/thumb/Features114/v4/57/af/66/" +
                        "57af661a-d217-7603-5e15-afdcca728f5b/mzl.alvvvari.jpg/1200x630cw.png")

        val info = ItunesArtistPageParser()
                .parse(HtmlDocumentLoader().load("applemusic-artist"))
        assertEquals(actual = info, expected = etalonInfo)
    }

    @Test
    fun parseAppleMusicAlbumnPage() {
        val etalonInfo = MusicMeta.Album(artist = "Solstafir",
                album = "Berdreyminn (Deluxe)",
                cover = "https://is3-ssl.mzstatic" +
                        ".com/image/thumb/Music113/v4/6d/fc/1f/" +
                        "6dfc1fed-8cce-4b58-e1c4-1c1df87984d9/886446421568.jpg/600x600bf.png")

        val info = ItunesAlbumTrackPageParser()
                .parse(HtmlDocumentLoader().load("applemusic-album"))
        assertEquals(actual = info, expected = etalonInfo)
    }

    @Test
    fun parseAppleMusicTrackPage() {
        //for some unknown reason all pages to track will liead toalbum instead
        val etalonInfo = MusicMeta.Album(artist = "Solstafir",
                album = "Berdreyminn (Deluxe)",
                cover = "https://is3-ssl.mzstatic" +
                        ".com/image/thumb/Music113/v4/6d/fc/1f/" +
                        "6dfc1fed-8cce-4b58-e1c4-1c1df87984d9/886446421568.jpg/600x600bf.png")

        val info = ItunesAlbumTrackPageParser()
                .parse(HtmlDocumentLoader().load("applemusic-track"))
        assertEquals(actual = info, expected = etalonInfo)
    }
}