package tgmusicbot

import org.jsoup.Jsoup

class HtmlDocumentLoader {
	fun load(name: String) = Jsoup.parse(this::class.java.classLoader.getResource("${name}.html")!!.readText())!!
}