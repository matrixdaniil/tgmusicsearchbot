package tgmusicbot

import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class SupportedUrlsTest {

    private val supportedUrls = RootPageParserSelector()

    private fun test(url: String) {
        assertTrue(supportedUrls.isSupported(url), message = url)
    }

    private fun testn(url: String) {
        assertFalse(supportedUrls.isSupported(url), message = url)
    }

    @Test
    fun shouldFindSupportedUrls() {
        supportedUrls.registerUrlPattern("youtu.be", ServiceName.YOUTUBE)
        supportedUrls.registerUrlPattern("youtube.com", ServiceName.YOUTUBE)
        supportedUrls.registerUrlPattern("music.youtube.com", ServiceName.YOUTUBEMUSIC)
        supportedUrls.registerUrlPattern("music.apple.com", null)

        test("https://youtu.be/kgPHwfra7ag")
        test("https://music.youtube.com/playlist?list=PL09E3064DB3CEAF2F")
        test("https://music.youtube.com/watch?v=_AnCxsxN3Lk&feature=share")
        test("https://music.apple.com/us/artist/in-flames/15866128")
        testn("http://myip.com")
    }
}