package tgmusicbot.youtube

import tgmusicbot.*

class YoutubeMusicLinkGenerator : ServiceLinkGenerator {
    override fun generate(item: MusicMeta) = ServiceLink(
            ServiceName.YOUTUBEMUSIC,
            "https://music.youtube.com/search?q=" + item.encodedSearchString()
    )
}