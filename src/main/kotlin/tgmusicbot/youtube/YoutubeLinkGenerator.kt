package tgmusicbot.youtube

import tgmusicbot.*

class YoutubeLinkGenerator : ServiceLinkGenerator {
    override fun generate(item: MusicMeta) = ServiceLink(
            ServiceName.YOUTUBE,
            "https://www.youtube.com/results?search_query=" + item.encodedSearchString()
    )
}