package main

import java.io.FileInputStream
import java.util.*

class Config(cfgPath: String) {

    private val cfgFile = Properties()

    init {
        cfgFile.load(FileInputStream(cfgPath))
    }

    fun getSpotifyClientSecret(): String = cfgFile.getProperty("spotify_client_secret")
    fun getSpotifyClientId(): String = cfgFile.getProperty("spotify_client_id")
    fun getTgToken(): String = cfgFile.getProperty("tg_token")
    fun getHelpFilePath(): String = cfgFile.getProperty("help_file")
}