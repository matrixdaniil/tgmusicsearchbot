package tgmusicbot

import me.ivmg.telegram.entities.ParseMode
import me.ivmg.telegram.entities.inlinequeryresults.InlineQueryResult
import me.ivmg.telegram.entities.inlinequeryresults.InputMessageContent

class SimpleMusicItemFormatter : MusicItemFormatter {
    override fun asMarkdown(musicItem: MusicItem): String {
        var result = asMarkdown(musicItem.meta)
        result += "\n\n"
        result += linksAsMarkdown(musicItem.urls)
        return result
    }

    override fun asArticle(musicItem: MusicItem): InlineQueryResult.Article {
        val meta = musicItem.meta
        val id = meta.hashCode().toString()
        return InlineQueryResult.Article(id = id,
                title = title(meta),
                inputMessageContent = InputMessageContent.Text(asMarkdown(musicItem), parseMode = ParseMode.MARKDOWN),
                thumbUrl = cover(meta)
        )
    }

    private fun title(meta: MusicMeta) = when (meta) {
        is MusicMeta.Artist -> "Artist: ${meta.artist}"
        is MusicMeta.Album -> "Album ${meta.album} by ${meta.artist}"
        is MusicMeta.Track -> "Track ${meta.track} by ${meta.artist}"
    }

    private fun cover(meta: MusicMeta) = when (meta) {
        is MusicMeta.Artist -> meta.cover
        is MusicMeta.Album -> meta.cover
        is MusicMeta.Track -> meta.cover
    }

    private fun linksAsMarkdown(urls: Collection<ServiceLink>): String {
        var result = "*Get music:*"
        urls.forEach {
            result += '\n'
            result += "[${it.serviceName.n}](${it.url})"
        }
        return result
    }

    private fun asMarkdown(meta: MusicMeta): String {
        var description = "*${title(meta)}*"

        description += '\n'

        if (meta is MusicMeta.Track)
            description += "\n*Album:* ${meta.album}"

        when (meta) {
            is MusicMeta.Track -> meta.duration
            is MusicMeta.Album -> meta.duration
            else -> null
        }?.let { description += "\n*Duration:* ${formatDuration(it)}" }

        cover(meta)?.let { description += "\n[cover image]($it)" }

        return description
    }

    private fun formatDuration(value: Int): String {
        var v = value
        val hrs: Int = v / 3600
        v %= 3600
        val minutes = v / 60
        v %= 60
        val seconds = v
        var str = ""
        if (hrs > 0)
            str = "%02d:".format(hrs)
        return str + "%02d:%02d".format(minutes, seconds)
    }

}