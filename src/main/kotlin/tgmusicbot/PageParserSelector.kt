package tgmusicbot

import java.util.*

interface PageParserSelector {
    fun select(url: String) : Optional<() -> PageParser>
}