package tgmusicbot

interface ServiceLinkGenerator {
    fun generate(item: MusicMeta): ServiceLink
}