package tgmusicbot

import me.ivmg.telegram.entities.inlinequeryresults.InlineQueryResult

interface MusicItemFormatter {
    fun asMarkdown(musicItem: MusicItem): String
    fun asArticle(musicItem: MusicItem): InlineQueryResult.Article
}