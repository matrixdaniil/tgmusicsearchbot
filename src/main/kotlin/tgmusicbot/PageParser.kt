package tgmusicbot

import org.jsoup.nodes.Document

interface PageParser {
	fun parse(html: Document): MusicMeta
}