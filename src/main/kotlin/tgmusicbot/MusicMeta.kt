package tgmusicbot

sealed class MusicMeta {
	data class Artist(
		var artist: String,
		var cover: String? = null
	) : MusicMeta()
	data class Album(
			var album: String,
			var artist: String,
			var duration: Int? = null,
			var cover: String? = null
	) : MusicMeta()
	data class Track(
			var track: String,
			var artist: String,
			var album: String? = null,
			var duration: Int? = null,
			var cover: String? = null
	) : MusicMeta()
}

