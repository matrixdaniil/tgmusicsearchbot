package tgmusicbot

import java.util.Optional

interface SupportedUrls {
    fun isSupported(url: String): Boolean
    fun serviceName(url: String): ServiceName?
    fun isWellSupported(url: String): Boolean
}