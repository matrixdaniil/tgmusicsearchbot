package tgmusicbot

enum class ServiceName(val n: String) {
    SPOTIFY("spotify"),
    APPLEMUSIC("apple music"),
    YOUTUBE("youtube"),
    YOUTUBEMUSIC("youtube music"),
    LASTFM("lastfm"),
    DEEZER("deezer")
}