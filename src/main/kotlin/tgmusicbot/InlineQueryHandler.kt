package tgmusicbot

import me.ivmg.telegram.Bot
import me.ivmg.telegram.entities.InlineQuery
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class InlineQueryHandler(private val musicSearcher: MusicSearcher,
                         private val executor: ScheduledExecutorService,
                         private val supportedUrls: SupportedUrls,
                         private val musicItemFormatter: MusicItemFormatter) {

    private val inlineTasks = HashMap<Long, ScheduledFuture<*>>()

    fun handle(bot: Bot, query: InlineQuery) {
        val userId = query.from.id
        val url = query.query.trim()
        val isUrl = UrlMatcher.matcher.matches(url)
        if (url.isEmpty())
            bot.answerInlineQuery(query.id)
        val waitTime: Long = if (isUrl) 500 else 1800
        inlineTasks[userId]?.cancel(true)
        inlineTasks[userId] = executor.schedule({
            (if (isUrl)
                musicSearcher.searchByUrl(url)
            else
                musicSearcher.searchByKeywords(query.query))
                        .whenComplete { results, exception ->
                            exception?.printStackTrace()
                            if (results.isNotEmpty()) {
                                bot.answerInlineQuery(
                                        query.id,
                                        results.map {
                                            musicItemFormatter.asArticle(it)
                                        }
                                )
                            } else {
                                bot.answerInlineQuery(
                                        query.id,
                                        switchPmText = "No results found",
                                        switchPmParameter = "help"
                                )
                            }
                        }
        }, waitTime, TimeUnit.MILLISECONDS)
    }
}