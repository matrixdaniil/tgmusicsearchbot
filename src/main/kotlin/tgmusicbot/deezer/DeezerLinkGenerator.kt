package tgmusicbot.deezer

import tgmusicbot.*

class DeezerLinkGenerator : ServiceLinkGenerator {
    override fun generate(item: MusicMeta) = ServiceLink(
            ServiceName.DEEZER,
            "https://www.deezer.com/search/" + item.encodedSearchString()
    )
}