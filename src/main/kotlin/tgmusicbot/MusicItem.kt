package tgmusicbot

data class MusicItem(
        val meta: MusicMeta,
        val urls: Collection<ServiceLink>
)