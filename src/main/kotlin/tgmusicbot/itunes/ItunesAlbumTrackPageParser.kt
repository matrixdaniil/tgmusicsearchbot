package tgmusicbot.itunes

import tgmusicbot.PageParser
import org.jsoup.nodes.Document
import tgmusicbot.MusicMeta

class ItunesAlbumTrackPageParser : PageParser {
	override fun parse(html: Document): MusicMeta {
		val re = Regex("(.+) by (.+)$")
		val title = html.head().selectFirst("meta[property=\"og:title\"]").attr("content")
		val type = html.head().selectFirst("meta[property=\"og:type\"]").attr("content")
		val cover = html.head().selectFirst("meta[name=\"twitter:image\"]").attr("content")
		val vals = re.matchEntire(title)!!.groupValues
		return when (type) {
			"music.album" -> MusicMeta.Album(artist = vals[2], album = vals[1], cover = cover)
			"music.song" -> MusicMeta.Track(artist = vals[2], track = vals[1], duration = parseDuration(html), cover = cover)
			else -> throw Exception("Unknown itunes music type")
		}
	}
	
	private fun parseDuration(html: Document) = html
				.head()
				.selectFirst("meta[property=\"music:duration\"]")
				.attr("content")
		    	.toInt()
}