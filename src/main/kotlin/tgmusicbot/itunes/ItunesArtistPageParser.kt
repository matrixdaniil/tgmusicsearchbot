package tgmusicbot.itunes

import tgmusicbot.PageParser
import org.jsoup.nodes.Document
import tgmusicbot.MusicMeta

class ItunesArtistPageParser : PageParser {
	override fun parse(html: Document): MusicMeta {
		val artistRe = Regex("(.+) on Apple Music")
		val title = html.head().selectFirst("meta[property=\"og:title\"]").attr("content")
		val cover = html.head().selectFirst("meta[name=\"twitter:image\"]").attr("content")
		val artist = artistRe.matchEntire(title)!!.groupValues[1]
		return MusicMeta.Artist(artist = artist, cover = cover)
	}
}