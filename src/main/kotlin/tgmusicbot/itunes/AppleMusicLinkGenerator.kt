package tgmusicbot.itunes

import tgmusicbot.*

class AppleMusicLinkGenerator : ServiceLinkGenerator {
    override fun generate(item: MusicMeta): ServiceLink {
        return ServiceLink(ServiceName.APPLEMUSIC,
                "https://music.apple.com/us/search?searchIn=am&term=" + item.encodedSearchString())
    }
}