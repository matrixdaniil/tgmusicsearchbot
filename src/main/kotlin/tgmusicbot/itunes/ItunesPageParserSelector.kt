package tgmusicbot.itunes

import tgmusicbot.PageParserSelector
import java.util.Optional
import tgmusicbot.PageParser

class ItunesPageParserSelector : PageParserSelector {
	    override fun select(url: String): Optional<() -> PageParser> {
        val pageTypeRe = Regex("https://music.apple.com/[^/]+/([^/]+)/.+")
        val matchResult = pageTypeRe.matchEntire(url) ?: return Optional.empty()
        val type = matchResult.groupValues[1]
        val parser:() -> PageParser = when (type) {
            "album" -> ::ItunesAlbumTrackPageParser
            "artist" -> ::ItunesArtistPageParser
            else -> return Optional.empty()
        }
        return Optional.of(parser)
    }

}