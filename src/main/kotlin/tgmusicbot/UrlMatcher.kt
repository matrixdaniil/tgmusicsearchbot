package tgmusicbot

object UrlMatcher {
    val matcher = Regex("^https?://(www\\.)?([^/]+)(/?\\??[a-zA-Z0-9&+_\\-=]+)+/?$")
}