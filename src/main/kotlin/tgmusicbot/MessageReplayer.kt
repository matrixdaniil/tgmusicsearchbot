package tgmusicbot

import me.ivmg.telegram.Bot
import me.ivmg.telegram.entities.Message
import me.ivmg.telegram.entities.ParseMode

class MessageReplayer(var replyWithErrors: Boolean, private val bot: Bot, private val message: Message) {

    fun msg(txt: String) {
        bot.sendMessage(chatId = message.chat.id, text = txt, replyToMessageId = message.messageId)
    }
    fun msgMd(txt: String) {
        bot.sendMessage(chatId = message.chat.id, text = txt, replyToMessageId = message.messageId, parseMode = ParseMode.MARKDOWN)
    }
    fun error(txt: String) {
        if (replyWithErrors)
            msg(txt)
    }
}