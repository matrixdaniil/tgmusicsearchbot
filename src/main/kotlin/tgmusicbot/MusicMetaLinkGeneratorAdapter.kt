package tgmusicbot

import java.net.URLEncoder

fun MusicMeta.encodedSearchString(): String {
    val url = when (this) {
        is MusicMeta.Artist -> artist
        is MusicMeta.Album -> "$artist $album"
        is MusicMeta.Track -> "$artist $track"
    }
    return URLEncoder.encode(url, "utf-8").replace("+", "%20")
}