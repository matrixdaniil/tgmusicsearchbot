package tgmusicbot

import java.util.concurrent.CompletableFuture

interface MusicSearchService {
    data class SearchResult(
            val meta: MusicMeta,
            val url: String
    )

    fun search(meta: MusicMeta, limit: Int = 1): CompletableFuture<List<SearchResult>>
    fun search(keywords: String, limit: Int = 10): CompletableFuture<List<SearchResult>>
}