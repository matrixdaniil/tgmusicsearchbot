package tgmusicbot.spotify

import tgmusicbot.*
import java.lang.Exception

class SpotifyLinkGenerator(private val spotifyService: MusicSearchService) : ServiceLinkGenerator {

    fun fallback(item: MusicMeta) = ServiceLink(
        ServiceName.SPOTIFY,
        "https://open.spotify.com/search/" + item.encodedSearchString()
    )

    override fun generate(item: MusicMeta): ServiceLink {
        try {
            val result = spotifyService.search(item).get()
            if (result.isEmpty())
                return fallback(item)
            return ServiceLink(ServiceName.SPOTIFY, result.first().url)
        } catch (e: Exception) {
            e.printStackTrace()
            return fallback(item)
        }
    }
}