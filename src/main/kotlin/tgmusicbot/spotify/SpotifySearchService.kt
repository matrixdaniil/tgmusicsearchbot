package tgmusicbot.spotify

import com.wrapper.spotify.SpotifyApi
import com.wrapper.spotify.exceptions.detailed.UnauthorizedException
import com.wrapper.spotify.model_objects.specification.*
import com.wrapper.spotify.requests.IRequest
import com.wrapper.spotify.requests.data.search.SearchItemRequest
import tgmusicbot.MusicMeta
import tgmusicbot.MusicSearchService
import java.lang.Exception
import java.util.concurrent.CompletableFuture
import kotlin.math.ceil

class SpotifySearchService(private val clientSecret: String, private val clientId: String) : MusicSearchService {

    private lateinit var serviceApi: SpotifyApi
    @Volatile
    private var tokenExpiresIn: Long = 0

    @Synchronized
    fun initialize() {
        if (this::serviceApi.isInitialized)
            return
        serviceApi = SpotifyApi.Builder()
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .build()
        requestToken()
    }

    private fun currentTime(): Long = System.currentTimeMillis()/1000

    private fun requestToken() {
        val token = serviceApi.clientCredentials().build().execute()
        serviceApi.accessToken = token.accessToken
        tokenExpiresIn = currentTime() + token.expiresIn
    }

    @Synchronized
    private fun refreshToken() {
        if (isTokenExpired())
            requestToken()
    }

    private fun isTokenExpired() = tokenExpiresIn < currentTime()

    private fun <E> executeRequestAsync(request: IRequest<E>) = CompletableFuture
            .supplyAsync { executeRequest(request) }

    private fun <E> executeRequest(request: IRequest<E>): E {
        if (isTokenExpired())
            refreshToken()
        return try {
            request.execute()
        } catch (e: UnauthorizedException) {
            refreshToken()
            request.execute()
        }
    }

    private class SearchRequestBuilder(private val api: SpotifyApi) {
        private var query: String = ""
        private var type: String = ""
        private var limit = 1

        private fun add(q: String) {
            if (query.isEmpty())
                query = q
            query += " $q"
        }

        fun artist(artist: String): SearchRequestBuilder {
            add("artist:$artist")
            return this
        }

        fun album(artist: String): SearchRequestBuilder {
            add("album:${artist}")
            return this
        }

        fun type(type: String) : SearchRequestBuilder {
            this.type = type
            return this
        }

        fun limit(limit: Int) : SearchRequestBuilder {
            this.limit = limit
            return this
        }

        fun rawQuery(query: String) : SearchRequestBuilder {
            this.query = query
            return this
        }

        fun build() : SearchItemRequest {
            if (type.isEmpty())
                type = "track,album,artist"
            return api.searchItem(query, type)
                    .limit(limit)
                    .build()
        }
    }

    private fun Array<Image>.mediumPreview() = find { it.width == 300 }?.url ?: find { it.width in 150..350 }?.url

    private fun pack(spotifyAlbums: Array<AlbumSimplified>): List<MusicSearchService.SearchResult> {
        return spotifyAlbums.map {
            val meta = MusicMeta.Album(
                    album = it.name,
                    artist = it.artists.joinToString { it.name },
                    cover = it.images.mediumPreview()
            )
            MusicSearchService.SearchResult(
                    meta = meta,
                    url = it.externalUrls["spotify"]
            )
        }
    }

    private fun pack(spotifyTracks: Array<Track>): List<MusicSearchService.SearchResult> {
        return spotifyTracks.map {
            val meta = MusicMeta.Track(
                    track = it.name,
                    artist = it.artists.joinToString { it.name },
                    cover = it.album.images.mediumPreview(),
                    duration = ceil(it.durationMs / 1000.0).toInt(),
                    album = it.album.name
            )
            MusicSearchService.SearchResult(
                    meta = meta,
                    url = it.externalUrls["spotify"]
            )
        }
    }

    private fun pack(spotifyArtists: Array<Artist>): List<MusicSearchService.SearchResult> {
        return spotifyArtists.map {
            val meta = MusicMeta.Artist(
                    artist = it.name,
                    cover = it.images.mediumPreview()
            )
            MusicSearchService.SearchResult(
                    meta = meta,
                    url = it.externalUrls["spotify"]
            )
        }
    }

    override fun search(meta: MusicMeta, limit: Int): CompletableFuture<List<MusicSearchService.SearchResult>> {
        when (meta) {
            is MusicMeta.Album -> {
                val request = SearchRequestBuilder(serviceApi)
                        .artist(meta.artist)
                        .album(meta.album)
                        .type("album")
                        .build()
                val result = executeRequestAsync(request)
                return result.thenApply { pack(it.albums.items) }
            }
            is MusicMeta.Track -> {
                var builder = SearchRequestBuilder(serviceApi)
                        .artist(meta.artist)
                        .album(meta.track)
                        .type("track")
                meta.album?.let { builder = builder.album(it) }
                val request = builder.build()
                val result = executeRequestAsync(request)
                return result.thenApply { pack(it.tracks.items) }
            }
            is MusicMeta.Artist -> {
                val request = SearchRequestBuilder(serviceApi)
                        .artist(meta.artist)
                        .type("artist")
                        .build()
                val result = executeRequestAsync(request)
                return result.thenApply { pack(it.artists.items) }
            }
            else -> throw Exception("not supported; wip")
        }
    }

    override fun search(keywords: String, limit: Int): CompletableFuture<List<MusicSearchService.SearchResult>> {
        val request = SearchRequestBuilder(serviceApi)
                .rawQuery(keywords)
                .limit(limit)
                .build()
        val result = executeRequestAsync(request)
        return result.thenApply {
            pack(it.tracks.items) +
            pack(it.albums.items) +
            pack(it.artists.items)
        }
    }
}