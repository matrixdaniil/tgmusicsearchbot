package tgmusicbot

import java.util.*
import kotlin.collections.HashMap

class RootPageParserSelector : PageParserSelector, SupportedUrls {
    private val pageParserSelectors = LinkedList<PageParserSelector>()
    private val urls = HashMap<String, ServiceName?>()
    private val allUrls = HashMap<String, ServiceName?>()

    fun registerPageParserSelector(selector: PageParserSelector) {
        pageParserSelectors.add(selector)
    }

    fun registerUrlPattern(urlPattern: String, serviceName: ServiceName?) {
        urls[urlPattern] = serviceName
        allUrls[urlPattern] = serviceName
    }

    fun registerWeakUrlPattern(urlPattern: String, serviceName: ServiceName?) {
        allUrls[urlPattern] = serviceName
    }

    override fun select(url: String): Optional<() -> PageParser> {
        for (selector in pageParserSelectors) {
            val oPparser = selector.select(url)
            if (oPparser.isPresent)
                return oPparser
        }
        return Optional.empty()
    }

    override fun isSupported(url: String): Boolean =
            UrlMatcher.matcher.matchEntire(url)?.groupValues?.let {
                allUrls.containsKey(it[2])
            } ?: false

    override fun isWellSupported(url: String): Boolean =
            UrlMatcher.matcher.matchEntire(url)?.groupValues?.let {
                urls.containsKey(it[2])
            } ?: false

    override fun serviceName(url: String): ServiceName? =
            UrlMatcher.matcher.matchEntire(url)?.groupValues?.let {
                allUrls[it[2]]
            }
}