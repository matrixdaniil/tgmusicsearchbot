package tgmusicbot

import org.jsoup.nodes.Document
import java.lang.Exception

class FallbackPageParser {
    sealed class ParseResult {
        data class Supported(
                val meta: MusicMeta
        ) : ParseResult()

        data class Usupported(
                val keywrods: String
        ) : ParseResult()
    }

    fun parse(html: Document): ParseResult {
        val type = html.head().selectFirst("meta[property=\"og:type\"]")?.attr("content")
        try {
            return when (type) {
                "music.album" -> ParseResult.Supported(parseAlbum(html))
                "music.song" -> ParseResult.Supported(parseTrack(html))
                "music.musician" -> ParseResult.Supported(parseArtist(html))
                else -> ParseResult.Usupported(parseOther(html))
            }
        } catch (e: Exception) {
            return ParseResult.Usupported(parseOther(html))
        }
    }

    private fun parseOther(html: Document): String {
        val title = html.head().selectFirst("meta[property=\"og:title\"]").attr("content")
        return title.replace("-", "")
    }

    private fun parseAlbum(html: Document): MusicMeta {
        val artist = html
                .head()
                .selectFirst("meta[property=\"twitter:audio:artist_name\"]")
                ?.attr("content") ?: html.head()
                .selectFirst("meta[name=\"twitter:audio:artist_name\"]")
                ?.attr("content")

        val album = html
                .head()
                .selectFirst("meta[property=\"twitter:title\"]")
                ?.attr("content") ?: html.head()
                .selectFirst("meta[name=\"twitter:title\"]")
                ?.attr("content")

        return MusicMeta.Album(artist = artist!!, album = album!!, cover = getCover(html))
    }

    private fun parseTrack(html: Document): MusicMeta {
        val artist = html
                .head()
                .selectFirst("meta[property=\"twitter:audio:artist_name\"]")
                ?.attr("content") ?: html.head()
                .selectFirst("meta[name=\"twitter:audio:artist_name\"]")
                ?.attr("content")

        val track = html
                .head()
                .selectFirst("meta[property=\"twitter:title\"]")
                ?.attr("content") ?: html.head()
                .selectFirst("meta[name=\"twitter:title\"]")
                ?.attr("content")

        val duration = html
                .head()
                .selectFirst("meta[property=\"music:duration\"]")
                .attr("content")

        return MusicMeta.Track(artist = artist!!, track = track!!, duration = duration.toInt(), cover = getCover(html))
    }

    private fun parseArtist(html: Document): MusicMeta {
        val artist = html
                .head()
                .selectFirst("meta[property=\"twitter:audio:artist_name\"]")
                ?.attr("content") ?: html.head()
                .selectFirst("meta[name=\"twitter:audio:artist_name\"]")
                ?.attr("content") ?: html.head()
                .selectFirst("meta[property=\"twitter:title\"]")
                ?.attr("content") ?: html.head()
                .selectFirst("meta[name=\"twitter:title\"]")
                ?.attr("content")

        return MusicMeta.Artist(artist = artist!!, cover = getCover(html))
    }

    private fun getCover(html: Document) =
            html
                    .head()
                    .selectFirst("meta[property=\"twitter:image\"]")
                    ?.attr("content") ?: html.head()
                    .selectFirst("meta[name=\"twitter:image\"]")
                    ?.attr("content") ?: html.head()
                    .selectFirst("meta[property=\"og:image\"]")
                    ?.attr("content")
}
