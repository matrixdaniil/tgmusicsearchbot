package tgmusicbot.lastfm

import tgmusicbot.*

class LastfmLinkGenerator : ServiceLinkGenerator {
    override fun generate(item: MusicMeta) = ServiceLink(
            ServiceName.LASTFM,
            "https://www.last.fm/search?q=" + item.encodedSearchString()
    )
}