package tgmusicbot

data class ServiceLink(
        val serviceName: ServiceName,
        val url: String
)
