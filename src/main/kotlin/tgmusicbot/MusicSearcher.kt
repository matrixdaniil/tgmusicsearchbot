package tgmusicbot

import java.util.concurrent.CompletableFuture

interface MusicSearcher {
    fun searchByUrl(url: String): CompletableFuture<List<MusicItem>>
    fun searchByKeywords(keywords: String): CompletableFuture<List<MusicItem>>
}