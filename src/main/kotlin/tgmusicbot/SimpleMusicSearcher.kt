package tgmusicbot

import okhttp3.OkHttpClient
import okhttp3.Request
import org.jsoup.Jsoup
import tgmusicbot.deezer.DeezerLinkGenerator
import tgmusicbot.itunes.AppleMusicLinkGenerator
import tgmusicbot.lastfm.LastfmLinkGenerator
import tgmusicbot.spotify.SpotifyLinkGenerator
import tgmusicbot.youtube.YoutubeLinkGenerator
import tgmusicbot.youtube.YoutubeMusicLinkGenerator
import java.lang.Exception
import java.util.*
import java.util.concurrent.CompletableFuture

class SimpleMusicSearcher(private val parserSelector: PageParserSelector,
                          private val supportedUrls: SupportedUrls,
                          spotifyLinkGenerator: SpotifyLinkGenerator,
                          private val spotifySearchService: MusicSearchService,
                          private val fallbackPageParser: FallbackPageParser) : MusicSearcher {

    private data class LinkGenerator(
            val generator: ServiceLinkGenerator,
            val serviceName: ServiceName
    )

    private inner class SearchResultMapper(private val searchServiceName: ServiceName,
                                           private val knownUrls: EnumMap<ServiceName, String> = EnumMap(ServiceName::class.java)) {

        fun mapSearchResult(searchResult: MusicSearchService.SearchResult): MusicItem {
            knownUrls[searchServiceName] = searchResult.url
            return MusicItem(
                    meta = searchResult.meta,
                    urls = getLinks(searchResult.meta, knownUrls)
            )
        }

    }

    private val linkGenerators = arrayOf(
            LinkGenerator(spotifyLinkGenerator, ServiceName.SPOTIFY),
            LinkGenerator(YoutubeLinkGenerator(), ServiceName.YOUTUBE),
            LinkGenerator(YoutubeMusicLinkGenerator(), ServiceName.YOUTUBEMUSIC),
            LinkGenerator(LastfmLinkGenerator(), ServiceName.LASTFM),
            LinkGenerator(DeezerLinkGenerator(), ServiceName.DEEZER),
            LinkGenerator(AppleMusicLinkGenerator(), ServiceName.DEEZER)
    )

    private fun getLinks(musicMeta: MusicMeta, knownUrls: Map<ServiceName, String>) =
            linkGenerators
                    .filter { !knownUrls.contains(it.serviceName) }
                    .map { it.generator.generate(musicMeta) } +
                    knownUrls.map { ServiceLink(it.key, it.value) }


    override fun searchByUrl(url: String): CompletableFuture<List<MusicItem>> {
        val oPageParserCreator = parserSelector.select(url)
        if (oPageParserCreator.isPresent) {
            return handleKnownUrl(oPageParserCreator, url)
        }
        return handleUnknownUrl(url)
    }

    private fun handleUnknownUrl(url: String): CompletableFuture<List<MusicItem>> {
        val result = fallbackPageParser.parse(parsePage(getPage(url)))
        val urlType = supportedUrls.serviceName(url)!!
        val knownUrls = EnumMap<ServiceName, String>(ServiceName::class.java)
        knownUrls[urlType] = url

        if (urlType == ServiceName.SPOTIFY && result is FallbackPageParser.ParseResult.Usupported)
            println("Error: spotify parser can't parse page $url")

        if (urlType != ServiceName.SPOTIFY || result is FallbackPageParser.ParseResult.Usupported) {
            return when (result) {
                is FallbackPageParser.ParseResult.Supported -> spotifySearchService.search(result.meta)
                is FallbackPageParser.ParseResult.Usupported -> spotifySearchService.search(result.keywrods)
            }.thenApply {
                if (it.isEmpty())
                    return@thenApply listOf<MusicItem>()
                val mapper = SearchResultMapper(ServiceName.SPOTIFY, knownUrls)
                it.map(mapper::mapSearchResult)
            }
        }
        if (result is FallbackPageParser.ParseResult.Supported) {
            return CompletableFuture.completedFuture(listOf(MusicItem(result.meta, getLinks(result.meta, knownUrls))))
        }
        throw Exception("this is impossible")
    }

    private fun handleKnownUrl(oPageParserCreator: Optional<() -> PageParser>,
                               url: String): CompletableFuture<List<MusicItem>> {
        val pageParser = oPageParserCreator.get().invoke()

        val musicMeta = pageParser.parse(parsePage(getPage(url)))
        val knownUrls = mapOf(Pair(supportedUrls.serviceName(url)!!, url))
        return CompletableFuture.completedFuture(listOf(MusicItem(
                musicMeta,
                getLinks(musicMeta, knownUrls)
        )))
    }

    override fun searchByKeywords(keywords: String): CompletableFuture<List<MusicItem>> {
        val mapper = SearchResultMapper(ServiceName.SPOTIFY)
        return spotifySearchService.search(keywords)
                .thenApply { it.map(mapper::mapSearchResult) }
    }

    private fun getPage(url: String): String {
        val client = OkHttpClient()
        val req = Request.Builder()
                .url(url)
                .build()

        val response = client.newCall(req).execute()
        val body = response.body() ?: throw Exception("Can't fetch the url $url")
        return body.string()
    }

    private fun parsePage(content: String) = Jsoup.parse(content)

}