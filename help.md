*Music Streaming services bot help*

*About*

Convert links from one music streaming service to others.
Sometimes it will generate direct urls to same item, but in most cases _for now_ just links to search page. Refer to supported services section.

*Usage*

Inline mode:
+ Enter one of the supported urls to a song, artist or an album: `@musstreamingbot https://open.spotify.com/track/6l2YlgrnMHCHUqPGs8kvwk`
+ Enter some keywords to search music by: `@musstreamingbot Solstafir Silfur-Refur`

Group chats:
Add a bot and it will convert urls for some services on the fly.

*Supported services*

Fully supported:
+ Spotify

Url/page parsing support (means metadata will be extracted):
+ Itunes
+ Deezer
+ Apple music

Url parsing to extract keywords only:
+ Youtube
+ Youtube music

Links to service which will be automatically parsed in group chats:
+ Spotify
+ Itunes
+ Deezer
+ Apple music
+ Youtube music (results can be inconsistent)

